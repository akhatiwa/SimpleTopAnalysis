#!/bin/bash

if [ $# != 2 ]; then
    echo "call as source run_ttbarCor.sh <InputFile_Listname> <Output_RootFilename>"
#    exit 1
fi

cp ttbarCor_v1.h ttbarCor.h
cp ttbarCor_v1.C ttbarCor.C

if [ ttbarCor ]; then 
     rm -r ttbarCor
fi

g++ -o ttbarCor  main_ttbarCor.cpp ttbarCor.C `root-config --cflags --glibs --libs` -I$ROOTSYS/include -g

INPUT=$1
OUTPUT=$2

./ttbarCor $INPUT $OUTPUT


