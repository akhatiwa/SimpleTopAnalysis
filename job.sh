#!/bin/bash
cd /afs/cern.ch/user/a/akhatiwa/work/private/TTbarCor/CMSSW_7_6_3/src/
cmsenv

cp ttbarCor_v1.h ttbarCor.h
cp ttbarCor_v1.C ttbarCor.C

rm -r ttbarCor

g++ -o ttbarCor  main_ttbarCor.cpp ttbarCor.C `root-config --cflags --glibs --libs`

INPUT="MC13TeV_TTJets_herwig_copy1.txt"
OUTPUT="outputfile.root"

./ttbarCor $INPUT $OUTPUT

