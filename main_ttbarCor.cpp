#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <ctime>
#include <TROOT.h>
#include <TUnixSystem.h>
#include <TChain.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TString.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include "ttbarCor.h"

using std::string;
using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;
using std::ios;

using std::vector;
using std::getline;

TTree *tree;
int main(int argc, char* argv[]) {
  
  if (argc !=3) {
    cout << "Wrong no. of arguments"<<endl;
    cout << "Please, call in the following format:" << endl;
    cout << "./ttbarCor <InputFileList> <OutputFile>" << endl;
    return 1;
  }
  
  ifstream inputlist;
  string outputfile(argv[2]);
  
  Int_t sumEntries = 0;
  
  
  if (argc == 3)
    {
      
      inputlist.open(argv[1]);
      if(!inputlist.is_open()){
        std::cout<<"error opening file\n";
        exit(0);
      }
      
      string filename;
      
      //Name of the tree in the file
      TChain* ch = new TChain("analysis/data");
      
      std::cout<<std::endl;
      std::cout<<"-----> merge File(s)"<<std::endl;
      
      
      while (!inputlist.eof()) {
	
        getline(inputlist,filename);
	
        std::cout<<"Filename: "<<filename.c_str()<<endl;
	
        if (filename == "") continue; // skip empty lines
	
	ch->Add(filename.c_str());
      }
      
      inputlist.clear();//restart reading input list from the beginning
      inputlist.seekg(0,ios::beg);

      ttbarCor a(ch);
            

      a.SetOutputFileName(outputfile);
      a.Start();
      a.Loop();
      a.End();
      
      inputlist.close();
      inputlist.clear();
      return 0;
    }
  
}


