//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Sep  7 22:48:43 2016 by ROOT version 6.02/13
// from TTree data/data
// found on file: root://eoscms//eos/cms/store/cmst3/user/psilva/LJets2016/8db9ad6/MC13TeV_TTJets_herwig/MergedMiniEvents_0.root
//////////////////////////////////////////////////////////

#ifndef ttbarCor_h
#define ttbarCor_h
#include "TH1.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TLorentzVector.h"

// Header file for the classes stored in the TTree if any.

class ttbarCor {
 public :
  TChain          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain
  
  TFile *fout; //output file to store histograms
  TLorentzVector lepton1;
  TLorentzVector lepton2;
  TLorentzVector bottom1;
  TLorentzVector bottom2;
  double HT;
  std::vector<TLorentzVector>leptons;
  std::vector<TLorentzVector>bottoms;
         
   // Declaration of leaf types
   Bool_t          isData;
   Int_t           run;
   Int_t           event;
   Int_t           lumi;
   Int_t           pu;
   Int_t           putrue;
   Int_t           ttbar_nw;
   Int_t           ttbar_allmepartons;
   Int_t           ttbar_matchmepartons;
   Float_t         ttbar_w[500];   //[ttbar_nw]
   Int_t           ngjets;
   Int_t           ngbjets;
   Int_t           ngleptons;
   Int_t           ng;
   Int_t           g_id[100];   //[ng]
   Float_t         g_pt[100];   //[ng]
   Float_t         g_eta[100];   //[ng]
   Float_t         g_phi[100];   //[ng]
   Float_t         g_m[100];   //[ng]
   Int_t           ngtop;
   Int_t           gtop_id[100];   //[ngtop]
   Float_t         gtop_pt[100];   //[ngtop]
   Float_t         gtop_eta[100];   //[ngtop]
   Float_t         gtop_phi[100];   //[ngtop]
   Float_t         gtop_m[100];   //[ngtop]
   Int_t           ngpf;
   Int_t           gpf_id[1000];   //[ngpf]
   Int_t           gpf_c[1000];   //[ngpf]
   Int_t           gpf_g[1000];   //[ngpf]
   Float_t         gpf_pt[1000];   //[ngpf]
   Float_t         gpf_eta[1000];   //[ngpf]
   Float_t         gpf_phi[1000];   //[ngpf]
   Float_t         gpf_m[1000];   //[ngpf]
   Int_t           nvtx;
   Int_t           muTrigger;
   Int_t           elTrigger;
   Int_t           nleptons;
   Int_t           nl;
   Bool_t          isPromptFinalState[100];   //[nl]
   Bool_t          isDirectPromptTauDecayProductFinalState[100];   //[nl]
   Int_t           l_id[100];   //[nl]
   Int_t           l_pid[100];   //[nl]
   Int_t           l_g[100];   //[nl]
   Int_t           l_charge[100];   //[nl]
   Float_t         l_pt[100];   //[nl]
   Float_t         l_eta[100];   //[nl]
   Float_t         l_phi[100];   //[nl]
   Float_t         l_mass[100];   //[nl]
   Float_t         l_scaleUnc[100];   //[nl]
   Float_t         l_chargedHadronIso[100];   //[nl]
   Float_t         l_miniIso[100];   //[nl]
   Float_t         l_relIso[100];   //[nl]
   Float_t         l_ip3d[100];   //[nl]
   Float_t         l_ip3dsig[100];   //[nl]
   Int_t           nj;
   Int_t           j_g[100];   //[nj]
   Float_t         j_area[100];   //[nj]
   Float_t         j_rawsf[100];   //[nj]
   Float_t         j_pt[100];   //[nj]
   Float_t         j_eta[100];   //[nj]
   Float_t         j_phi[100];   //[nj]
   Float_t         j_mass[100];   //[nj]
   Float_t         j_csv[100];   //[nj]
   Float_t         j_csvl[100];   //[nj]
   Float_t         j_cvsb[100];   //[nj]
   Float_t         j_vtxpx[100];   //[nj]
   Float_t         j_vtxpy[100];   //[nj]
   Float_t         j_vtxpz[100];   //[nj]
   Float_t         j_vtxmass[100];   //[nj]
   Int_t           j_vtxNtracks[100];   //[nj]
   Float_t         j_vtx3DVal[100];   //[nj]
   Float_t         j_vtx3DSig[100];   //[nj]
   Float_t         j_puid[100];   //[nj]
   Int_t           j_flav[100];   //[nj]
   Int_t           j_hadflav[100];   //[nj]
   Int_t           j_pid[100];   //[nj]
   Int_t           npf;
   Int_t           pf_j[1000];   //[npf]
   Int_t           pf_id[1000];   //[npf]
   Int_t           pf_c[1000];   //[npf]
   Float_t         pf_pt[1000];   //[npf]
   Float_t         pf_eta[1000];   //[npf]
   Float_t         pf_phi[1000];   //[npf]
   Float_t         pf_m[1000];   //[npf]
   Float_t         pf_puppiWgt[1000];   //[npf]
   Int_t           nmet;
   Float_t         met_pt[2];   //[nmet]
   Float_t         met_phi[2];   //[nmet]

   // List of branches
   TBranch        *b_isData;   //!
   TBranch        *b_run;   //!
   TBranch        *b_event;   //!
   TBranch        *b_lumi;   //!
   TBranch        *b_pu;   //!
   TBranch        *b_putrue;   //!
   TBranch        *b_ttbar_nw;   //!
   TBranch        *b_ttbar_allmepartons;   //!
   TBranch        *b_ttbar_matchmepartons;   //!
   TBranch        *b_ttbar_w;   //!
   TBranch        *b_ngjets;   //!
   TBranch        *b_ngbjets;   //!
   TBranch        *b_ngleptons;   //!
   TBranch        *b_ng;   //!
   TBranch        *b_g_id;   //!
   TBranch        *b_g_pt;   //!
   TBranch        *b_g_eta;   //!
   TBranch        *b_g_phi;   //!
   TBranch        *b_g_m;   //!
   TBranch        *b_ngtop;   //!
   TBranch        *b_gtop_id;   //!
   TBranch        *b_gtop_pt;   //!
   TBranch        *b_gtop_eta;   //!
   TBranch        *b_gtop_phi;   //!
   TBranch        *b_gtop_m;   //!
   TBranch        *b_ngpf;   //!
   TBranch        *b_gpf_id;   //!
   TBranch        *b_gpf_c;   //!
   TBranch        *b_gpf_g;   //!
   TBranch        *b_gpf_pt;   //!
   TBranch        *b_gpf_eta;   //!
   TBranch        *b_gpf_phi;   //!
   TBranch        *b_gpf_m;   //!
   TBranch        *b_nvtx;   //!
   TBranch        *b_muTrigger;   //!
   TBranch        *b_elTrigger;   //!
   TBranch        *b_nleptons;   //!
   TBranch        *b_nl;   //!
   TBranch        *b_isPromptFinalState;   //!
   TBranch        *b_isDirectPromptTauDecayProductFinalState;   //!
   TBranch        *b_l_id;   //!
   TBranch        *b_l_pid;   //!
   TBranch        *b_l_g;   //!
   TBranch        *b_l_charge;   //!
   TBranch        *b_l_pt;   //!
   TBranch        *b_l_eta;   //!
   TBranch        *b_l_phi;   //!
   TBranch        *b_l_mass;   //!
   TBranch        *b_l_scaleUnc;   //!
   TBranch        *b_l_chargedHadronIso;   //!
   TBranch        *b_l_miniIso;   //!
   TBranch        *b_l_relIso;   //!
   TBranch        *b_l_ip3d;   //!
   TBranch        *b_l_ip3dsig;   //!
   TBranch        *b_nj;   //!
   TBranch        *b_j_g;   //!
   TBranch        *b_j_area;   //!
   TBranch        *b_j_rawsf;   //!
   TBranch        *b_j_pt;   //!
   TBranch        *b_j_eta;   //!
   TBranch        *b_j_phi;   //!
   TBranch        *b_j_mass;   //!
   TBranch        *b_j_csv;   //!
   TBranch        *b_j_csvl;   //!
   TBranch        *b_j_cvsb;   //!
   TBranch        *b_j_vtxpx;   //!
   TBranch        *b_j_vtxpy;   //!
   TBranch        *b_j_vtxpz;   //!
   TBranch        *b_j_vtxmass;   //!
   TBranch        *b_j_vtxNtracks;   //!
   TBranch        *b_j_vtx3DVal;   //!
   TBranch        *b_j_vtx3DSig;   //!
   TBranch        *b_j_puid;   //!
   TBranch        *b_j_flav;   //!
   TBranch        *b_j_hadflav;   //!
   TBranch        *b_j_pid;   //!
   TBranch        *b_npf;   //!
   TBranch        *b_pf_j;   //!
   TBranch        *b_pf_id;   //!
   TBranch        *b_pf_c;   //!
   TBranch        *b_pf_pt;   //!
   TBranch        *b_pf_eta;   //!
   TBranch        *b_pf_phi;   //!
   TBranch        *b_pf_m;   //!
   TBranch        *b_pf_puppiWgt;   //!
   TBranch        *b_nmet;   //!
   TBranch        *b_met_pt;   //!
   TBranch        *b_met_phi;   //!

   ttbarCor(TChain *chain=0);
   virtual ~ttbarCor();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TChain *chain);
   virtual void     Loop();
   virtual void     Start();   
   virtual void     Reset();   
   virtual void     End();
   virtual void     SetOutputFileName(std::string outputfilename);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

};

bool  sortByPt(TLorentzVector i, TLorentzVector j);
float deltaR(float eta1, float phi1, float eta2, float phi2);
float dphi(float phi1, float phi2);

#endif

#ifdef ttbarCor_cxx
ttbarCor::ttbarCor(TChain *chain) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (chain == 0) {
        std::cout<<"error no tree"<<std::endl;
        exit(EXIT_FAILURE);
 
      //TDirectory * dir = (TDirectory*)f->Get("root://eoscms//eos/cms/store/cmst3/user/psilva/LJets2016/8db9ad6/MC13TeV_TTJets_herwig/MergedMiniEvents_0.root:/analysis");
      //dir->GetObject("data",tree);
   }
   Init(chain);
   Reset();
}

 

ttbarCor::~ttbarCor()
{
   if (!fChain) return;
   delete 
fChain->GetCurrentFile();
}

Int_t ttbarCor::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ttbarCor::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ttbarCor::Init(TChain *chain)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).



  // Set branch addresses and branch pointers
  if (!chain) return;
  fChain = chain;
  fCurrent = -1;
  fChain->SetMakeClass(1);


   fChain->SetBranchAddress("isData", &isData, &b_isData);
   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("lumi", &lumi, &b_lumi);
   fChain->SetBranchAddress("pu", &pu, &b_pu);
   fChain->SetBranchAddress("putrue", &putrue, &b_putrue);
   fChain->SetBranchAddress("ttbar_nw", &ttbar_nw, &b_ttbar_nw);
   fChain->SetBranchAddress("ttbar_allmepartons", &ttbar_allmepartons, &b_ttbar_allmepartons);
   fChain->SetBranchAddress("ttbar_matchmepartons", &ttbar_matchmepartons, &b_ttbar_matchmepartons);
   fChain->SetBranchAddress("ttbar_w", ttbar_w, &b_ttbar_w);
   fChain->SetBranchAddress("ngjets", &ngjets, &b_ngjets);
   fChain->SetBranchAddress("ngbjets", &ngbjets, &b_ngbjets);
   fChain->SetBranchAddress("ngleptons", &ngleptons, &b_ngleptons);
   fChain->SetBranchAddress("ng", &ng, &b_ng);
   fChain->SetBranchAddress("g_id", g_id, &b_g_id);
   fChain->SetBranchAddress("g_pt", g_pt, &b_g_pt);
   fChain->SetBranchAddress("g_eta", g_eta, &b_g_eta);
   fChain->SetBranchAddress("g_phi", g_phi, &b_g_phi);
   fChain->SetBranchAddress("g_m", g_m, &b_g_m);
   fChain->SetBranchAddress("ngtop", &ngtop, &b_ngtop);
   fChain->SetBranchAddress("gtop_id", gtop_id, &b_gtop_id);
   fChain->SetBranchAddress("gtop_pt", gtop_pt, &b_gtop_pt);
   fChain->SetBranchAddress("gtop_eta", gtop_eta, &b_gtop_eta);
   fChain->SetBranchAddress("gtop_phi", gtop_phi, &b_gtop_phi);
   fChain->SetBranchAddress("gtop_m", gtop_m, &b_gtop_m);
   fChain->SetBranchAddress("ngpf", &ngpf, &b_ngpf);
   fChain->SetBranchAddress("gpf_id", gpf_id, &b_gpf_id);
   fChain->SetBranchAddress("gpf_c", gpf_c, &b_gpf_c);
   fChain->SetBranchAddress("gpf_g", gpf_g, &b_gpf_g);
   fChain->SetBranchAddress("gpf_pt", gpf_pt, &b_gpf_pt);
   fChain->SetBranchAddress("gpf_eta", gpf_eta, &b_gpf_eta);
   fChain->SetBranchAddress("gpf_phi", gpf_phi, &b_gpf_phi);
   fChain->SetBranchAddress("gpf_m", gpf_m, &b_gpf_m);
   fChain->SetBranchAddress("nvtx", &nvtx, &b_nvtx);
   fChain->SetBranchAddress("muTrigger", &muTrigger, &b_muTrigger);
   fChain->SetBranchAddress("elTrigger", &elTrigger, &b_elTrigger);
   fChain->SetBranchAddress("nleptons", &nleptons, &b_nleptons);
   fChain->SetBranchAddress("nl", &nl, &b_nl);
   fChain->SetBranchAddress("isPromptFinalState", isPromptFinalState, &b_isPromptFinalState);
   fChain->SetBranchAddress("isDirectPromptTauDecayProductFinalState", isDirectPromptTauDecayProductFinalState, &b_isDirectPromptTauDecayProductFinalState);
   fChain->SetBranchAddress("l_id", l_id, &b_l_id);
   fChain->SetBranchAddress("l_pid", l_pid, &b_l_pid);
   fChain->SetBranchAddress("l_g", l_g, &b_l_g);
   fChain->SetBranchAddress("l_charge", l_charge, &b_l_charge);
   fChain->SetBranchAddress("l_pt", l_pt, &b_l_pt);
   fChain->SetBranchAddress("l_eta", l_eta, &b_l_eta);
   fChain->SetBranchAddress("l_phi", l_phi, &b_l_phi);
   fChain->SetBranchAddress("l_mass", l_mass, &b_l_mass);
   fChain->SetBranchAddress("l_scaleUnc", l_scaleUnc, &b_l_scaleUnc);
   fChain->SetBranchAddress("l_chargedHadronIso", l_chargedHadronIso, &b_l_chargedHadronIso);
   fChain->SetBranchAddress("l_miniIso", l_miniIso, &b_l_miniIso);
   fChain->SetBranchAddress("l_relIso", l_relIso, &b_l_relIso);
   fChain->SetBranchAddress("l_ip3d", l_ip3d, &b_l_ip3d);
   fChain->SetBranchAddress("l_ip3dsig", l_ip3dsig, &b_l_ip3dsig);
   fChain->SetBranchAddress("nj", &nj, &b_nj);
   fChain->SetBranchAddress("j_g", j_g, &b_j_g);
   fChain->SetBranchAddress("j_area", j_area, &b_j_area);
   fChain->SetBranchAddress("j_rawsf", j_rawsf, &b_j_rawsf);
   fChain->SetBranchAddress("j_pt", j_pt, &b_j_pt);
   fChain->SetBranchAddress("j_eta", j_eta, &b_j_eta);
   fChain->SetBranchAddress("j_phi", j_phi, &b_j_phi);
   fChain->SetBranchAddress("j_mass", j_mass, &b_j_mass);
   fChain->SetBranchAddress("j_csv", j_csv, &b_j_csv);
   fChain->SetBranchAddress("j_csvl", j_csvl, &b_j_csvl);
   fChain->SetBranchAddress("j_cvsb", j_cvsb, &b_j_cvsb);
   fChain->SetBranchAddress("j_vtxpx", j_vtxpx, &b_j_vtxpx);
   fChain->SetBranchAddress("j_vtxpy", j_vtxpy, &b_j_vtxpy);
   fChain->SetBranchAddress("j_vtxpz", j_vtxpz, &b_j_vtxpz);
   fChain->SetBranchAddress("j_vtxmass", j_vtxmass, &b_j_vtxmass);
   fChain->SetBranchAddress("j_vtxNtracks", j_vtxNtracks, &b_j_vtxNtracks);
   fChain->SetBranchAddress("j_vtx3DVal", j_vtx3DVal, &b_j_vtx3DVal);
   fChain->SetBranchAddress("j_vtx3DSig", j_vtx3DSig, &b_j_vtx3DSig);
   fChain->SetBranchAddress("j_puid", j_puid, &b_j_puid);
   fChain->SetBranchAddress("j_flav", j_flav, &b_j_flav);
   fChain->SetBranchAddress("j_hadflav", j_hadflav, &b_j_hadflav);
   fChain->SetBranchAddress("j_pid", j_pid, &b_j_pid);
   fChain->SetBranchAddress("npf", &npf, &b_npf);
   fChain->SetBranchAddress("pf_j", pf_j, &b_pf_j);
   fChain->SetBranchAddress("pf_id", pf_id, &b_pf_id);
   fChain->SetBranchAddress("pf_c", pf_c, &b_pf_c);
   fChain->SetBranchAddress("pf_pt", pf_pt, &b_pf_pt);
   fChain->SetBranchAddress("pf_eta", pf_eta, &b_pf_eta);
   fChain->SetBranchAddress("pf_phi", pf_phi, &b_pf_phi);
   fChain->SetBranchAddress("pf_m", pf_m, &b_pf_m);
   fChain->SetBranchAddress("pf_puppiWgt", pf_puppiWgt, &b_pf_puppiWgt);
   fChain->SetBranchAddress("nmet", &nmet, &b_nmet);
   fChain->SetBranchAddress("met_pt", met_pt, &b_met_pt);
   fChain->SetBranchAddress("met_phi", met_phi, &b_met_phi);
   Notify();
}

Bool_t ttbarCor::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ttbarCor::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ttbarCor::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ttbarCor_cxx
